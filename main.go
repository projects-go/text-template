package main

import (
	"os"
	"text/template"
)

type Persona struct {
	Nombre string
	Edad int
	Pais string
}
/*
const tp = 
`{{range .}}
	{{if .Edad}}
		Nombre:{{.Nombre}}, Edad: {{.Edad}} - Correcto
	{{else if .Nombre}}
		Nombre:{{.Nombre}}, Edad: {{.Edad}} - Falta edad
	{{else}}
		Nombre:{{.Nombre}}, Edad: {{.Edad}} - Falta nombre y edad
	{{end}}
{{end}}`
*/

func main() {
	/*
	personas := []Persona {
		Persona{"Osvaldo", 32},
		Persona{"Arnaldo", 34},
		Persona{"Leo", 35},
		Persona{"Maria", 0},
		Persona{"", 0},
		Persona{"Luis", 36},
	}
	*/
	persona := Persona{"Arnaldo", 34, "Argentina"}
	t := template.New("persona")
	//t, err := t.Parse(tp)
	t, err := t.ParseGlob("templates/*.txt")
	if err != nil {
		panic(err)
	}
	// err = t.Execute(os.Stdout, personas)
	err = t.ExecuteTemplate(os.Stdout, "residentes", persona)
	if err != nil {
		panic(err)
	}
	err = t.ExecuteTemplate(os.Stdout, "visitantes", persona)
	if err != nil {
		panic(err)
	}
}